def get_dictionary(keyfile, mode):
    file_handle = open(keyfile)
    dict = {}
    for line in file_handle.readlines():
        words = line.split()
        if len(words) != 2:
            raise ValueError('Too many words')
        unencoded = words[0]
        if len(unencoded) != 1:
            raise ValueError('Unencoded has too many letters: [' + unencoded + ']')
        encoded = words[1]
        if len(encoded) != 1:
            raise ValueError('Encoded has too many letters: [' + encoded + ']')
        if mode == 'encode':
            dict[unencoded] = encoded
        else:
            dict[encoded] = unencoded
    return dict