import random
import string
import blacklist

def get_unique_chars(inputfile):
    file_handle = open(inputfile)
    unique_chars = []
    for line in file_handle.readlines():
        new_chars = list(line)
        unique_chars.extend(new_chars)
        chars = set(unique_chars)
        unique_chars = list(chars)
    return unique_chars

def encode_chars(filtered_chars):
    dict = {}
    for filtered_char in filtered_chars:
        dict[filtered_char] = random.choice(string.ascii_letters)
    return dict

def generate(inputfile):
    unique_chars = get_unique_chars(inputfile)
    filtered_chars = [unique_char for unique_char in unique_chars if blacklist.not_blacklisted(unique_char)]
    return encode_chars(filtered_chars)
    