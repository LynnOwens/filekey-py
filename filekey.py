# Filekey
import argparse
import keygen
import translator

parser = argparse.ArgumentParser('Encodes and decodes a file using simple character replacement.  If a keyfile is not provided, a key for the input file will be generated and sent to stdout.  If a keyfile is provided, the input file will be decoded and sent to stdout.')
parser.add_argument('-k', metavar='keyfile', help='Use this keyfile to decode an encoded file', dest='keyfile')
parser.add_argument('-m', metavar='mode', help='encode or decode', dest='mode')
parser.add_argument('-n', metavar='input file', help='Contextually encode or decode this file', dest='inputfile', required=True)
args = parser.parse_args()

if args.keyfile is None:
    key_dict = keygen.generate(args.inputfile)
    for plain, encoded in key_dict.items():
        print(plain, encoded)
else:
    if args.mode != 'encode' and args.mode != 'decode':
        raise ValueError('Mode ' + args.mode + ' is not valid')
    translator.translate(args.keyfile, args.inputfile, args.mode)