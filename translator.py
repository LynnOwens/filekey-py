import blacklist
import dictionary

def translate(keyfile, inputfile, mode):
    dict = dictionary.get_dictionary(keyfile, mode)
    file_handle = open(inputfile)
    for line in file_handle.readlines():
        letters = list(line)
        new_line = ''
        for letter in letters:
            if blacklist.not_blacklisted(letter):
                encoded = dict[letter]
            elif letter == '\n':
                continue
            else:
                encoded = letter
            new_line += encoded
        print(new_line)